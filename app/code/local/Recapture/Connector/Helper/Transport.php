<?php

class Recapture_Connector_Helper_Transport extends Mage_Core_Helper_Abstract {

    public function dispatch($route = '', $data = array(), $storeId = null, $timeout = 2) {
        if (empty($route)) {
            return false;
        }

        try {
            $adapter = new Zend_Http_Client_Adapter_Curl();
            $client  = new Zend_Http_Client(Mage::helper('recapture')->getHomeUrl('beacon/' . $route), array(
                'timeout' => $timeout
            ));

            //this is the users publicly accessible session ID
            $data['customer'] = Mage::helper('recapture')->getCustomerHash();
            $data['timestamp'] = time();

            $client->setParameterPost($data);
            $client->setAdapter($adapter);
            $client->setHeaders('Api-Key', Mage::helper('recapture')->getApiKey($storeId));
            $response = $client->request('POST');

            return $response;

        } catch (Exception $e){
            return false;
        }
    }

    public function dispatchWithLongTimeout($route = '', $data = array()) {
        return $this->dispatch($route, $data, null, 60);
    }
}
