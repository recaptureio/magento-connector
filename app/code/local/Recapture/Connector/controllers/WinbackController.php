<?php

// http://localhost:8080/recapture/winback/?hash=1d2be268d9645e88d78eaf54b4539f33114a583c0a3d651dd6934383d8b99c1ad0dbc90606b18c930351df1ef28aa1b654a076f59d25980fa79dc5c82930d8a6

class Recapture_Connector_WinbackController extends Mage_Core_Controller_Front_Action {
    public function redirectWithUtm($url) {
        return $this->_redirect(
            $url, array('_query'=>$_SERVER['QUERY_STRING'])
        );
    }

    public function indexAction() {
        $cart = Mage::helper('checkout/cart')->getCart();
        $cart->truncate();

        $recapture = Mage::helper('recapture');
        $hash = $this->getRequest()->getParam('hash');
        $orderId = $recapture->translateOrderHash($hash);

        try {
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
        } catch (Exception $e){
            Mage::log($e->getMessage());
        }

        if (!$order->getId()) {
            return $this->redirectWithUtm('/');
        }

        $items = $order->getItemsCollection();
 
        foreach ($items as $item) {
            $cart->addOrderItem($item);
        }
 
        $cart->save();
        $redirectSection = $recapture->getReturnLanding();


        switch ($redirectSection){
            case Recapture_Connector_Model_Landing::REDIRECT_HOME:
                return $this->redirectWithUtm('/');

            case Recapture_Connector_Model_Landing::REDIRECT_CHECKOUT:
                $url = Mage::helper('checkout/url')->getCheckoutUrl().'?'.$_SERVER['QUERY_STRING'];
                return $this->getResponse()->setRedirect($url);

            case Recapture_Connector_Model_Landing::REDIRECT_CART:
            default:
                return $this->redirectWithUtm('checkout/cart');
        }
    }
}
