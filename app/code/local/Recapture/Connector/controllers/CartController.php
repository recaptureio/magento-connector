<?php

class Recapture_Connector_CartController extends Mage_Core_Controller_Front_Action {

    public function redirectWithUtm($url) {
        return $this->_redirect(
            $url, array('_query'=>$_SERVER['QUERY_STRING'])
        );
    }

    private function applyDiscountCode() {
        try {
            $discountCode = $this->getRequest()->getParam('discount');

            if ($discountCode != '') {
                Mage::getSingleton("checkout/session")->setData("coupon_code", $discountCode);
                Mage::getSingleton('checkout/cart')->getQuote()->setCouponCode($discountCode)->save();
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
    }

    public function indexAction(){

        $helper = Mage::helper('recapture');

        if (!$helper->isEnabled() || !$helper->getApiKey())
            return $this->redirectWithUtm('/');

        $hash = $this->getRequest()->getParam('hash');
        $cartId = null;

        try {
            $cartId = $helper->translateCartHash($hash);
        } catch (Exception $e){
            Mage::log($e->getMessage());
        }

        if (!$cartId){
            Mage::getSingleton('core/session')->addError('There was an error retrieving your cart.');
            return $this->redirectWithUtm('/');
        }

        try {
            $result = $helper->associateCartToMe($cartId);
        } catch (Exception $e){
            Mage::log($e->getMessage());
        }

        if (!$result){
            Mage::getSingleton('core/session')->addError('There was an error retrieving your cart.');
            return $this->redirectWithUtm('/');
        } else {
            // apply any discount codes
            $this->applyDiscountCode();

            $cart = Mage::getModel('checkout/cart')->getQuote();
            $redirectSection = $helper->getReturnLanding();

            switch ($redirectSection){
                case Recapture_Connector_Model_Landing::REDIRECT_HOME:
                    return $this->redirectWithUtm('/');

                case Recapture_Connector_Model_Landing::REDIRECT_CHECKOUT:
                    $url = Mage::helper('checkout/url')->getCheckoutUrl().'?'.$_SERVER['QUERY_STRING'];
                    return $this->getResponse()->setRedirect($url);

                case Recapture_Connector_Model_Landing::REDIRECT_CART:
                default:
                    return $this->redirectWithUtm('checkout/cart');
            }

            return $this->redirectWithUtm('/');
        }
    }
}