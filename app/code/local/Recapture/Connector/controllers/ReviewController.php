<?php

class Recapture_Connector_ReviewController extends Mage_Core_Controller_Front_Action {

    public function indexAction(){
        
        $orderId = false;
        $helper = Mage::helper('recapture');
        if (!$helper->isEnabled() || !$helper->getApiKey()) return $this->_redirect('/');

        $hash = $this->getRequest()->getParam('hash');

        try {
            $orderId = $helper->translateOrderHash($hash);
        } catch (Exception $e){
            Mage::log($e->getMessage());
        }

        if (!$orderId){
            Mage::getSingleton('core/session')->addError('There was an error retrieving your order.');
            return $this->_redirect('/');
        }
        
        $this->loadLayout();
        $this->getLayout()->getBlock('recapture.review.view')->setOrderId($orderId);
        $this->getLayout()->getBlock('recapture.review.view')->setActionUrl(Mage::getUrl('recapture/review/submit', array('hash' => $hash)));
        $this->renderLayout();
    }
    
    public function submitAction(){
        
        $orderId = false;
        $helper  = Mage::helper('recapture');
        $session = Mage::getSingleton('core/session');
        if (!$helper->isEnabled() || !$helper->getApiKey()) return $this->_redirect('/');

        $hash = $this->getRequest()->getParam('hash');

        try {

            $orderId = $helper->translateOrderHash($hash);

        } catch (Exception $e){

            Mage::log($e->getMessage());

        }

        if (!$orderId){

            Mage::getSingleton('core/session')->addError('There was an error retrieving your order.');
            return $this->_redirect('/');

        }
        
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		$items = $this->getRequest()->getParam('items');
        
        $submittedReviews = array();
        
        if (!$helper->isEnabled() || !$helper->getApiKey()) return $this->_redirect('/');
		
		foreach ($items as $productId => $item){
			
			if ($item['skip'] == 1) continue;

			$data = array();
			$data['title'] = $item['title'];
			$data['detail'] = $item['detail'];
            $data['sku'] = $item['sku'];
            $data['name'] = $item['name'];
			$data['nickname'] = empty($item['nickname']) ? 'N/A' : $item['nickname'];
            $data['ratings'] = array();
            $data['product_url'] = $item['product_url'];
            $data['customer_id'] = $order->getCustomerId();
			$ratings = $item['ratings'];
            
			$review = Mage::getModel('review/review')->setData($data);
			
            $review->setEntityId($review->getEntityIdByCode(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE))
                ->setCustomerId($order->getCustomerId())
				->setEntityPkValue($productId)
				->setStatusId(Mage_Review_Model_Review::STATUS_PENDING)
				->setStoreId($item['store_id'])
				->setStores(array($item['store_id']))
				->save();
			
			foreach ($ratings as $ratingId => $optionId) {
                
                $rating = Mage::getModel('rating/rating')->load($ratingId);
                
                $options = $rating->getOptions();
                
                foreach ($options as $option){
                    if ($option->getOptionId() == $optionId){
                        $data['ratings'][] = array(
                            'label' => $rating->getRatingCode(),
                            'value' => $option->getValue()
                        );
                        break;
                    }
                }
                
				Mage::getModel('rating/rating')
				->setRatingId($ratingId)
				->setReviewId($review->getId())
				->addOptionVote($optionId, $productId);
			}
			
			$review->aggregate();
            
            $submittedReviews[] = $data;
		
		}
        
        $transportData = array(
            'external_id' => $orderId,
            'reviews'     => $submittedReviews
        );
        
        Mage::helper('recapture/transport')->dispatchWithLongTimeout(
            'order/review',
            $transportData
        );
		
		$this->_redirect('recapture/review/success');
    }
    
    public function successAction(){
        
        $this->loadLayout();
        $this->renderLayout();
        
    }

}