<?php

class Recapture_Connector_Block_Review extends Mage_Core_Block_Template {
    
    private $_ratings    = null;
    private $_order      = null;
    private $_orderItems = null;
    
    
	public function _beforeToHtml(){
        
        if (empty($this->_order)){
            
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($this->getOrderId());
            
        }
        
        if (empty($this->_orderItems)){
            
            $this->_orderItems = $this->_order->getAllVisibleItems();
            
        }
		
	}
    
    
    public function getOrder(){
        
        return $this->_order;
        
    }
    
    
    public function getRatings(){
        
		if (empty($this->_ratings)){
		
			$ratingCollection = Mage::getModel('rating/rating')
				->getResourceCollection()
				->addEntityFilter('product')
				->setPositionOrder()
				->addRatingPerStoreName(Mage::app()->getStore()->getId())
				->setStoreFilter(Mage::app()->getStore()->getId())
				->load()
				->addOptionToItems();
				
			$this->_ratings = $ratingCollection;
		
		}
			
        return $this->_ratings;
    }
    
    public function getOrderItems(){
        
        return $this->_orderItems;
        
    }
    
}