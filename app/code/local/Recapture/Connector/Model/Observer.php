<?php

class Recapture_Connector_Model_Observer {

    public function quoteUpdate($observer){

        if (!Mage::helper('recapture')->isEnabled()) return $this;

        try {

          return $this->_updateQuote($observer->getEvent()->getQuote());

        } catch (Exception $e){

          Mage::log($e->getMessage());

        }

        return $this;

    }

    protected function _updateQuote(Mage_Sales_Model_Quote $quote){

        if (!Mage::helper('recapture')->isEnabled()) return $this;

        if (!$quote->getId()) return;

        if (Mage::helper('recapture')->isIpIgnored()) return $this;
        
        $mediaConfig = Mage::getModel('catalog/product_media_config');
        $storeId     = Mage::app()->getStore();

        $totalWithTax = Mage::getStoreConfig('recapture/abandoned_carts/include_tax_with_products');
    
        $customerGroup = Mage::getModel('customer/group')->load($quote->getCustomerGroupId());

        $transportData = array(
            'first_name'          => Mage::helper('recapture')->getCustomerFirstname($quote),
            'last_name'           => Mage::helper('recapture')->getCustomerLastname($quote),
            'email'               => Mage::helper('recapture')->getCustomerEmail($quote),
            'customer_group'      => $customerGroup->getCustomerGroupCode(),
            'external_id'         => $quote->getId(),
            'grand_total'         => Mage::helper('core')->currency($quote->getBaseGrandTotal(), false, false),
            'grand_total_display' => Mage::helper('core')->currency($quote->getBaseGrandTotal(), true, false),
            'products'            => array(),
            'totals'              => array()
        );

        $cartItems = $quote->getAllVisibleItems();

        foreach ($cartItems as $item){
            $productModel = $item->getProduct();
            $productImage = false;

            // get the product categories
            $categories = $productModel
                ->getCategoryCollection()
                ->addAttributeToSelect('name');

            $categoryNames = [];
            $categoryIds = [];

            foreach ($categories as $cat) {
                $parents = $cat->getCollection()
                    ->addIdFilter($cat->getParentIds())
                    ->addAttributeToSelect('name')
                    ->addUrlRewriteToResult()
                    ->setOrder('level');

                foreach ($parents as $parentCat) {
                    $categoryIds[$parentCat->getId()] = $parentCat->getId();
                    $categoryNames[$parentCat->getName()] = $parentCat->getName();
                }

                $categoryIds[$cat->getId()] = $cat->getId();
                $categoryNames[$cat->getName()] = $cat->getName();
            }

            // get the image
            $image = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productModel->getId(), 'thumbnail', $storeId);
            if ($image && $image != 'no_selection') $productImage = $mediaConfig->getMediaUrl($image);

            // check configurable first
            if ($item->getProductType() == 'configurable'){

                if (Mage::getStoreConfig('checkout/cart/configurable_product_image') == 'itself'){

                    $child = $productModel->getIdBySku($item->getSku());

                    $image = Mage::getResourceModel('catalog/product')->getAttributeRawValue($child, 'thumbnail', $storeId);
                    if ($image && $image != 'no_selection')
                        $productImage = $mediaConfig->getMediaUrl($image);
                }
            }

            // then check grouped
            Mage::log('Recapture: Magento grouped product setting: ' . Mage::getStoreConfig('checkout/cart/grouped_product_image'));

            if (Mage::getStoreConfig('checkout/cart/grouped_product_image') == 'parent'){
                Mage::log('Recapture: Looking for parent products for ' . $productModel->getId());
                $options = $productModel->getTypeInstance(true)->getOrderOptions($productModel);

                Mage::log('Recapture: Product Options' . json_encode($options));

                if (isset($options['super_product_config']) && $options['super_product_config']['product_type'] == 'grouped'){

                    $parent = $options['super_product_config']['product_id'];
                    $image = Mage::getResourceModel('catalog/product')->getAttributeRawValue($parent, 'thumbnail', $storeId);

                    Mage::log('Recapture: Found group image ' . json_encode($image));

                    if ($image && $image != 'no_selection')
                        $productImage = $mediaConfig->getMediaUrl($image);
                }

                if (!$productImage) {
                    $parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($productModel->getId());

                    if ($parentIds && isset($parentIds[0])) {
                        Mage::log('Recapture: Found parent products: ' . json_encode($parentIds));
                        $image = Mage::getResourceModel('catalog/product')->getAttributeRawValue($parentIds[0], 'thumbnail', $storeId);

                        if ($image)
                            Mage::log('Recapture: Got product image ' . $image);
                        else
                            Mage::log('Recapture: No image found');

                        if ($image && $image != 'no_selection')
                            $productImage = $mediaConfig->getMediaUrl($image);

                    } else {
                        Mage::log('Recapture: No parent products found for');
                    }
                }
            }

            //if after all that, we still don't have a product image, we get the placeholder image
            if (!$productImage) {
                $productImage = $mediaConfig->getMediaUrl('placeholder/' . Mage::getStoreConfig("catalog/placeholder/image_placeholder"));
            }

            $optionsHelper = Mage::helper('catalog/product_configuration');

            if ($item->getProductType() == 'configurable'){

                $visibleOptions = $optionsHelper->getConfigurableOptions($item);

            } else {

                $visibleOptions = $optionsHelper->getCustomOptions($item);

            }

            /*
            'grand_total'         => Mage::helper('core')->currency($quote->getBaseGrandTotal(), false, false),
            'grand_total_display' => Mage::helper('core')->currency($quote->getBaseGrandTotal(), true, false),
            */

            $price = $totalWithTax
                ? $item->getBasePriceInclTax()
                : $item->getBasePrice();

            $product = array(
                'name' => $item->getName(),
                'sku' => $item->getSku(),
                'price' => Mage::helper('core')->currency($price, false, false),
                'price_display' => Mage::helper('core')->currency($price, true, false),
                'qty' => $item->getQty(),
                'image' => $productImage,
                'options' => $visibleOptions,
                'category_names'=> array_values($categoryNames),
                'category_ids'=> array_values($categoryIds)
            );

            $transportData['products'][] = $product;

        }

        $totals = $quote->getTotals();

        foreach ($totals as $total){

            $value = $total->getValue();

            //we pass grand total on the top level
            if ($total->getCode() == 'grand_total' ||
                is_array($value) ||
                is_object($value)) {
                continue;
            }

            $total = array(
                'name'   => $total->getTitle(),
                'amount' => (string)$value
            );

            $transportData['totals'][] = $total;

        }

        Mage::helper('recapture/transport')->dispatch('cart', $transportData);

        return $this;

    }

    public function quoteDelete($observer){

        if (!Mage::helper('recapture')->isEnabled()) return $this;

        try {

            $quote = $observer->getEvent()->getQuote();

            $transportData = array(
                'external_id'  => $quote->getId()
            );

            Mage::helper('recapture/transport')->dispatch('cart/remove', $transportData);

        } catch (Exception $e){

            Mage::log($e->getMessage());

        }

        return $this;

    }

    public function cartConversion($observer){

        if (!Mage::helper('recapture')->isEnabled()) return $this;

        try {

            $order = $observer->getEvent()->getOrder();
            $order_id = $order->getIncrementId();
            $shipping_address = $order->getShippingAddress();
            $billing_address = $order->getBillingAddress();

            $transportData = array(
                'external_id' => $order->getQuoteId(),
                'order_id' => $order_id,
                'shipping_address' => $shipping_address
                    ? $shipping_address->getData()
                    : null,
                'billing_address' => $billing_address
                    ? $billing_address->getData()
                    : null,
            );

            if (!is_null($order_id)) {
                Mage::helper('recapture/transport')->dispatch('conversion', $transportData);
            }
        } catch (Exception $e){
            Mage::log($e->getMessage());
        }

        return $this;

    }


    public function orderUpdate($observer){

        $order = $observer->getEvent()->getOrder();

        if ($order->getState() == $order->getOrigData('state')) return $this;

        try {

            $transportData = array(
                'external_id' => $order->getIncrementId(),
                'state'       => $order->getState()
            );

            Mage::helper('recapture/transport')->dispatch('order/update', $transportData, $order->getStoreId());

        } catch (Exception $e){

            Mage::log($e->getMessage());

        }

        return $this;

    }


    public function newsletterSubscriber($observer){

        if (!Mage::helper('recapture')->isEnabled()) return $this;
        if (!Mage::helper('recapture')->shouldCaptureSubscriber()) return $this;

        //if we can't identify this customer, we return out
        if (!Mage::helper('recapture')->getCustomerHash()) return $this;

        try {

            $subscriber = $observer->getEvent()->getSubscriber();

            $email = $subscriber->getSubscriberEmail();

            if (Zend_Validate::is($email, 'EmailAddress')){

                $transportData = array(
                    'email'  => $email
                );

                Mage::helper('recapture/transport')->dispatch('email/subscribe', $transportData);

            }

        } catch (Exception $e){

            Mage::log($e->getMessage());

        }

        return $this;

    }

}