#!/usr/bin/env bash

build="v1.5.14"

# Make sure we are in an m1 install folder
if [ ! -f app/Mage.php ]
then
  echo "This script must be run from within the Magent 1 install folder"
  exit 1
fi

install_path="$PWD"
echo "Install location is $install_path"

# Download the extension
echo "Downloading the latest version of the extension"
rm -rf /tmp/recapture-magneto
cd /tmp
rm -f recapture-magento.tar.gz
curl "https://bitbucket.org/recaptureio/magento-connector/get/$build.tar.gz" -o recapture-magento.tar.gz >/dev/null 2>&1

# Unzip the extension
echo "Unzipping archive contents"
mkdir /tmp/recapture-magneto
tar xzf recapture-magento.tar.gz -C /tmp/recapture-magneto

cd /tmp/recapture-magneto
cd recapture*

echo "Copying extension local files"
rm -rf "$install_path/app/code/local/Recapture"
mkdir -p "$install_path/app/code/local/Recapture"
cp -R ./app/code/local/Recapture "$install_path/app/code/local"
chmod -R 777 "$install_path/app/code/local/Recapture"

echo "Copying extension layout files"
rm -rf "$install_path/app/design/frontend/base/default/layout/recapture.xml"
mkdir -p "$install_path/app/design/frontend/base/default/layout/"
cp -R ./app/design/frontend/base/default/layout/recapture.xml "$install_path/app/design/frontend/base/default/layout"

echo "Copying extension templates"
rm -rf "$install_path/app/design/frontend/base/default/template/recapture/"
mkdir -p "$install_path/app/design/frontend/base/default/template/"
cp -R ./app/design/frontend/base/default/template/recapture/ "$install_path/app/design/frontend/base/default/template/"
chmod -R 777 "$install_path/app/design/frontend/base/default/template/"

echo "Copying extension modules"
rm -rf "$install_path/app/etc/modules/Recapture_Connector.xml"
mkdir -p "$install_path/app/etc/modules"
cp -R ./app/etc/modules/Recapture_Connector.xml "$install_path/app/etc/modules"

echo "Copying extension skins"
rm -rf "$install_path/skin/frontend/base/default/recapture"
mkdir -p "$install_path/skin/frontend/base/default/"
cp -R ./skin/frontend/base/default/* "$install_path/skin/frontend/base/default/"
chmod -R 777 "$install_path/skin/frontend/base/default/"

echo "Cleaning up temp files"
rm -rf /tmp/recapture-magneto
rm -rf recapture-magento.tar.gz

echo "Done!"
echo "IMPORTANT! Please clear your Magento cache to complete the installation"
